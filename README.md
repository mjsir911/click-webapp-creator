## Dependencies
* `jq`
* `python3`
* `curl`

## Compiling

Must set variables `URL` and `MAINTAINER`, `$MAINTAINER` must be in email
format eg "Marco Sirabella `<test@example.com>`", and `$URL` must be set to the
website you are wanting to make a webapp for. `$URL` must not include http
in it.


```
URL=example.com MAINTAINER="Marco Sirabella <test@example.com>" ./doit.sh
```


## Installing

On host machine:
```
$ adb push output.click
$ adb shell pkcon install-local --allow-untrusted output.click
```
