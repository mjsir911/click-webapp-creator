#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

from html.parser import HTMLParser
import fileinput

__appname__     = "test"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""

class ManifestParser(HTMLParser):
    title = False

    def handle_starttag(self, tag, attrs):
        if tag == "title":
            self.title = True

    def handle_endtag(self, tag):
        if tag == "title":
            self.title = False

    def handle_data(self, data):
        if self.title:
            print(data)




parser = ManifestParser()


for line in fileinput.input():
    parser.feed(line)
