#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

from html.parser import HTMLParser
import fileinput

__appname__     = "test"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""

class ManifestParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        attrs = dict(attrs)
        if tag == "link":
            if attrs["rel"] == "manifest":
                print(attrs["href"])


parser = ManifestParser()


for line in fileinput.input():
    parser.feed(line)
