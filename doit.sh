#!/usr/bin/env bash

DATA_DIR=$(mktemp -d)
CONTROL_DIR=$(mktemp -d)
ICONS_LIST=$(mktemp)
ARCHIVE_DIRECTORY=$(mktemp -d)
MANIFEST=$(mktemp)

function cleanup {
	rm -rf $DATA_DIR $CONTROL_DIR $ICONS_LIST $ARCHIVE_DIRECTORY $MANIFEST
}

trap cleanup EXIT

export CURLOPTS="--location --silent"

NAME=$(curl $CURLOPTS $URL | python3 extract_title.py)

NAME_ESCAPED=$(tr '[:upper:]' '[:lower:]' <<< ${NAME} | tr -d ' ' | tr -d '[:punct:]')

curl $CURLOPTS $URL/$(curl $CURLOPTS $URL | python3 extract_manifest.py) > $MANIFEST

BACKGROUND_COLOR=$(jq '.background_color' -r < $MANIFEST)

jq ".icons" < $MANIFEST > $ICONS_LIST

LARGEST_ICON=$(cat $ICONS_LIST | jq ".[].sizes" --raw-output | sort -g | tail -1 )

ICONS_DIR_TMP=$(mktemp -d)
ICON_URL=$URL/$(cat $ICONS_LIST | jq ".[] | select(.sizes == \"$LARGEST_ICON\").src" --raw-output)
ICON_FILE=$(cd $ICONS_DIR_TMP 2> /dev/null && curl $CURLOPTS "$ICON_URL" -O && ls)

cp $ICONS_DIR_TMP/$ICON_FILE $DATA_DIR/$ICON_FILE


cat > ${DATA_DIR}/${NAME_ESCAPED}.apparmor << EOF
{
    "template": "ubuntu-webapp",
    "policy_groups": [
        "networking",
        "webview"
    ],
    "policy_version": 1.3
}
EOF

cat > ${DATA_DIR}/${NAME_ESCAPED}.desktop << EOF
[Desktop Entry]
Name=${NAME}
Comment=${NAME}
Type=Application
Icon=${ICON_FILE}
Exec=webapp-container --webappUrlPatterns=https?://${URL}/* https://${URL}
Terminal=false
X-Ubuntu-Touch=true
X-Ubuntu-Splash-Color=$BACKGROUND_COLOR
EOF

SIZE=$(($(du -k -s --apparent-size $DATA_DIR | cut -f1)+1))

VERSION_NUM=0.4

PACKAGENAME=${NAME_ESCAPED}.webapp

cat > ${CONTROL_DIR}/control << EOF
Package: $PACKAGENAME
Version: $VERSION_NUM
Click-Version: 0.4
Architecture: all
Maintainer: ${MAINTAINER}
Installed-Size: ${SIZE}
Description: ${NAME}
EOF

cat > ${CONTROL_DIR}/manifest << EOF
{
    "description": "${NAME}",
    "framework": "ubuntu-sdk-15.04.6",
    "hooks": {
        "${NAME_ESCAPED}": {
            "apparmor": "${NAME_ESCAPED}.apparmor",
            "desktop": "${NAME_ESCAPED}.desktop"
        }
    },
    "installed-size": "${SIZE}",
    "maintainer": "${MAINTAINER}",
    "name": "${PACKAGENAME}",
    "title": "${NAME}",
    "version": "$VERSION_NUM"
}
EOF

cat > ${CONTROL_DIR}/preinst << EOF
#! /bin/sh
echo "Click packages may not be installed directly using dpkg."
echo "Use 'click install' instead."
exit 1
EOF

(cd $DATA_DIR && md5sum *) > ${CONTROL_DIR}/md5sums

(cd $DATA_DIR    && tar c .) | gzip > $ARCHIVE_DIRECTORY/data.tar.gz
(cd $CONTROL_DIR && tar c .) | gzip > $ARCHIVE_DIRECTORY/control.tar.gz

	cat > $ARCHIVE_DIRECTORY/_click-binary << EOF
0.4
EOF

cat > $ARCHIVE_DIRECTORY/debian-binary << EOF
2.0
EOF

(cd $ARCHIVE_DIRECTORY && ar r output.click debian-binary _click-binary control.tar.gz data.tar.gz)
cp $ARCHIVE_DIRECTORY/output.click .
